-- Lexer File
-- Conor O'Neill

module Parser where

import Data.Char
import Data.Maybe
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import qualified Text.ParserCombinators.Parsec.Token as T
import Text.ParserCombinators.Parsec.Language
import Debug.Trace

-- data Message = Identifer String
--             |



casperDef = emptyDef
         { commentLine     = "--"
         , reservedNames   = ["#Free variables","InverseKeys =","#Processes","knows","generates","#Protocol description",
                            "#Specification","Secret","StrongSecret","Agreement","NonInjectiveAgreement","WeakAgreement",
                            "Aliveness","TimedAgreement","TimedNonInjectiveAgreement","TimedWeakAgreement","TimedAliveness",
                            "then","and","or","previously","sends","receives","message","from","to","containing","for",
                            "#Equivalences","forall","#Actual variables","External","InternalKnown","InternalUnknown",
                            "TimeStamp","MaxRunTime","#Functions","symbolic","#System","WithdrawOption","True","False",
                            "GenerateSystem = True","GenerateSystemForRepeatSection=","to","#Intruder Information","Intruder",
                            "IntruderKnowledge","UnboundParallel = True","IntruderProcesses","StaleKnowledge","Crackable",
                            "Guessable","#Channels","authenticated","secret","direct","C","NF","NRA","NRA-","NR","NR-",
                            "Session","Stream","injective","symmetric","#Simplifications","RemoveFields","RemoveHashedFields",
                            "RemoveEncryption","RemoveHash","SwapPairs","Coalesce","RemovePlainAndEnc"]
         , reservedOpNames = ["->","\\","(+)","%","=","|-"]
         , caseSensitive   = True
         }

lexer = T.makeTokenParser casperDef

integer     = T.integer lexer
symbol      = T.symbol lexer
parens      = T.parens lexer
sqBracks    = T.brackets lexer
angles      = T.angles lexer
braces      = T.braces lexer
semic       = T.semi lexer
comma       = T.comma lexer
dot         = T.dot lexer
colon       = T.colon lexer
commaSep    = T.commaSep lexer
semiSep     = T.semiSep lexer

reserved    = T.reserved lexer
reservedOp  = T.reservedOp lexer
whiteSpace  = T.whiteSpace lexer


-- Atom is either a single id OR id, id , list of id(possibly empty)
-- TODO Will either be singleton list or list of ids



-- Basic Def
identifier = do -- Returns single string
                c <- letter
                cs <- many (letter <|> digit )
                return (c:cs)

atom =      do -- Returns single id or list of ids
                a <- identifier
                return (Atom [a])
       <|>
            do
                b <- fn_app
                return (Atom b)

fn_app = do -- Returns list of ids
             f <- identifier
             fs <- parens fn_app'
             return (f:fs)

fn_app' = do -- Returns list of ids
            i <- identifier
            i' <- commaSep identifier
            return (i:i')

msg =       do
            i <- identifier
            m <- msg'
            m' <- msg''
            return (i,m,m')
        <|> do
            m <- parens msg
            m' <- msg''
            return (m,m')
        <|>
            do
            m <- braces msg
            a <- braces atom
            m' <- msg''
            return (m,a,m')

msg' =      do
            m <- parens msg
            return m
        <|> do
            p <- reservedOp "%"
            m <- msg
            return (p,m)
        <|>
            do
            spaces
            return []

msg'' =     do
            comma
            m <- msg
            m' <- msg''
            return (m,m')
        <|>
            do
            p <- reservedOp "%"
            i <- identifier
            m <- msg''
            return (p,i,m)
        <|> do
            x <- reservedOp "(+)"
            m <- msg
            m' <- msg''
            return (x,m,m')
        <|>
            do
            spaces
            return []

type_name = do
                t <- identifier
                return t

process_name = do
                    p <- identifier
                    return p

-- Main Script Parsing
script = do
                f <- free_vars
                proc <- processes
                prot <- prot_desc
                spec <- specification
                eq <-       do
                                eq <- equiv
                                return eq
                     <|>    do
                                spaces
                                return []
                act <- actual_vars
                func <-     do
                                func <- functions
                                return func
                     <|>
                            do
                                spaces
                                return []
                sys <- system
                int <- intruder
                ch <- channels
                return (f,proc,prot,spec,eq,act,func,sys,int,ch)

free_vars = do
                reserved "#Free variables"
                char '\n'
                v <- many (var_dec <|> inv_keys_dec)
                return v

var_dec =   do
                id <- identifier
                idx <- commaSep identifier
                colon
                t <- type_expr
                s <-    do
                            s <- subtype_expr
                            return s
                    <|>
                        do
                            spaces
                            return []
                char '\n'
                return (id,idx,t,s)


type_expr =     do
                    t <- type_name
                    t' <- type_expr'
                    return (t : t')


type_expr' =       do -- TODO type name = identifier
                        t <- sepBy type_name (char 'x')
                        reservedOp "->"
                        t' <- type_name
                        return (t ++ [t'])
                <|>
                    do
                        spaces
                        return []

inv_keys_dec =  do
                reserved "InverseKeys ="
                p <- inv_key_pair
                px <- commaSep inv_key_pair
                return (p,px)

inv_key_pair = do
                   i <- parens inv_key_pair'
                   return i

inv_key_pair' = do
                    i <- identifier
                    comma
                    i' <- identifier
                    return (i,i')

-- Processes Section
processes = do
            reserved "#Processes"
            char '\n'
            xs <- many process_def
            return xs

process_def = do
                  name <- process_name
                  xs <- parens process_def'
                  k <-      do
                                k <- knows_stmt
                                return k
                       <|>  do
                                spaces
                                return []
                  g <-      do
                                g <- gen_stmt
                                return g
                       <|>
                            do
                                spaces
                                return []

                  return (name,xs,k,g)

process_def' = do
                    i <- identifier
                    is <- commaSep identifier
                    return (i:is)

knows_stmt = do
                reserved "knows"
                a <- atom
                a' <- commaSep atom
                return (a,a')
gen_stmt = do
            reserved "generates"
            i <- identifier
            i' <- commaSep identifier
            return (i,i')

-- Protocol Steps Section

prot_desc = do
                reserved "#Protocol Description"
                char '\n'
                p <- many prot_desc'
                return p

prot_desc' =    do
                    p <- prot_msg
                    return p
            <|> do
                    p <- env_msg_send
                    return p
            <|> do
                    p <- env_msg_rec
                    return p
prot_msg =  do
                line <-     do
                                line <- assignment_line
                                char '\n'
                                return line
                        <|> do
                                spaces
                                return []
                ln <- line_no
                dot
                e <- identifier
                reservedOp "->"
                e' <- identifier
                colon
                m <- msg
                char '\n'
                tl <-   do
                            tl <- test_line
                            char '\n'
                            return tl
                    <|> do
                            spaces
                            return []
                return (line,ln,e,e',m,tl)

assignment_line = do
                  al <- angles assignment_line'
                  return al

assignment_line' = do
                        a <- assignment
                        a' <- semiSep assignment
                        return (a:a')

assignment =    do
                    i <- identifier
                    reservedOp ":="
                    fdr_expr -- TODO
                    return i
fdr_expr =  do -- TODO
                whiteSpace
                return []

test_line =     do
                   t <- sqBracks test
                   return t

test =      do
                f <- fdr_expr
                return f

env_msg_rec =   do
                    ln <- line_no
                    dot
                    reservedOp "->"
                    i <- identifier
                    colon
                    m <- msg
                    char '\n'
                    tl <-   do
                                tl <- test_line
                                return tl
                        <|> do
                                spaces
                                return []
                    return (ln,i,m,tl)
env_msg_send =  do
                    aline <-    do
                                    aline <- assignment_line
                                    char '\n'
                                    return aline
                            <|> do
                                    spaces
                                    return []
                    line_num <- line_no
                    dot
                    i <- identifier
                    reservedOp "->"
                    colon
                    m <- msg
                    char '\n'
                    return (aline,line_num, i, m)

line_no =   do
                x <- integer <|> letter
                xs <- many ( integer <|> letter )
                return (x:xs)

specification =     do
                        reserved "Specification"
                        specs <- many (spec <|> temporal_spec)
                        return specs

spec =      do
                reserved "Secret"
                a <- parens spec'
                char '\n'
                return a
        <|> do
                reserved "StrongSecret"
                a <- parens spec'
                char '\n'
                return a
        <|> do
                reserved "Agreement"
                a <- parens spec''
                char '\n'
                return a
        <|> do
                reserved "NonInjectiveAgreement"
                a <- parens spec''
                char '\n'
                return a
        <|> do
                reserved "WeakAgreement"
                a <- parens spec'''
                char '\n'
                return a
        <|> do
                reserved "Aliveness"
                a <- parens spec'''
                char '\n'
                return a
        <|> do
                reserved "TimedAgreement"
                a <- parens spec''''
                char '\n'
                return a
        <|> do
                reserved "TimedNonInjectiveAgreement"
                a <- parens spec''''
                char '\n'
                return a
        <|> do
                reserved "TimedWeakAgreement"
                a <- parens spec'''''
                char '\n'
                return a
        <|> do
                reserved "TimedAliveness"
                a <- parens spec'''''
                char '\n'
                return a

spec' = do
            i <- identifier
            semic
            a <- atom
            semic
            ag <- agents
            return (i:a:ag)

spec'' = do
            i <- identifier
            semic
            i' <- identifier
            semic
            f <- fields
            return (i,i',f)

spec''' = do
            i <- identifier
            semic
            i' <- identifier
            return (i:i')

spec'''' = do
                i <- identifier
                semic
                i' <- identifier
                semic
                t <- time
                semic
                f <- fields
                return (i,i',t,f)

spec''''' = do
                i <- identifier
                semic
                i' <- identifier
                semic
                t <- time
                return (i,i',t)

agents = do
            a <- sqBracks agents'
            return a

agents' = do
            i <- identifier
            is <- commaSep identifier
            return (i:is)

fields = do
            f <-    do
                        f <- sqBracks fields'
                        return f
                <|>
                    do
                        sqBracks spaces
                        return []
            return f

fields' =   do
                i <- identifier
                is <- commaSep identifier
                return (i:is)


time = do
            t <- digit
            return t

temporal_spec = do
                    reserved "if"
                    t <- temporal_formula
                    reserved "then"
                    t' <- temporal_formula
                    return (t:t')



temporal_formula =      do
                        tf <- parens temporal_formula
                        t' <- temporal_formula'
                        return (tf, t')
                    <|> do
                        p <- reserved "previously"
                        tf <- temporal_formula'
                        return (p,tf)
                    <|>
                        do
                        te <- temporal_event
                        t' <- temporal_forumla'
                        return (te,t')

temporal_formula' =     do
                        a <- reserved "and"
                        tf <- temporal_formula
                        t' <- temporal_formula'
                        return (a,tf,t')
                    <|> do
                        a <- reserved "or"
                        tf <- temporal_formula
                        t' <- temporal_formula'
                        return (a,tf,t')
                    <|> do
                        spaces
                        return []
temporal_event =    do
                        i <- identifier
                        sr <- send_rec
                        m <- reserved "message"
                        l <- line_no
                        ft <-   do
                                    ft <- from_to
                                    return ft
                            <|> do
                                    spaces
                                    return []
                        i' <-   do
                                    i' <- identifier
                                    return i'
                            <|>
                                do
                                    spaces
                                    return []
                        c <-    do
                                    c <-reserved "containing"
                                    return c
                            <|>
                                do
                                    spaces
                                    return []
                        s <-    do
                                    s <- sub
                                    return s
                            <|>
                                do
                                    spaces
                                    return []
                        s' <-   do
                                    s' <- commaSep sub
                                    return s'
                            <|>
                                do
                                    spaces
                                    return []
                        return (i,sr,m,l,ft,i',c,s,s')

-- Helpers for above
send_rec =      do
                    s <- reserved "sends"
                    return s
           <|>  do
                    s <- reserved "receives"
                    return s

from_to  =      do
                    s <- reserved "from"
                    return s
           <|>  do
                    s <- reserved "to"
                    return s

sub =   do
            i <- identifier
            i' <- sub'
            return (i:i')

sub' =      do
                whiteSpace
                return []
        <|>
            do
                r <- reserved "for"
                id <- identifier
                return (r:id)

-- Equivalences Section

equiv =     do
                reserved "#Equivalences"
                char '\n'
                e <- many equiv_dev
                return e

equiv_dev =     do
                    reserved "forall"
                    q <- quants
                    dot
                    m <- msg
                    reservedOp "="
                    m' <- msg
                    char '\n'
                    return (q,m,m')

quants =    do
                q <- quant
                qs <- semiSep quant
                return (q:qs)

quant =     do
                i <- identifier
                is <- commaSep identifier
                t <-    do
                            t <- quant'
                            return t
                    <|> do
                            spaces
                            return []
                return (i:is:t)

quant' = do
            colon
            t <- type_name
            return t



-- Actual Variables Section

actual_vars = do
                reserved "Actual Variables"
                char '\n'
                a <- many act_dec
                return a

act_dec =   do
                    a <- act_var_dec
                    return a
                <|>
                    do
                    a <- timestamp_def
                    return a
                <|>
                    do
                    a <- maxruntime_def
                    return a
                <|>
                    do
                    a <- act_inv_keys_dec
                    return a

act_var_dec =   do
                    i <- identifier
                    ix <- commaSep identifier
                    colon
                    t <- type_name
                    d <-    do
                                d <- di_tag
                                return d
                        <|>
                            do
                                spaces
                                return []
                    s <-    do
                                s <- subtype_expr
                                return s
                        <|>
                            do
                                spaces
                                return []
                    return (i,ix,t,d,s)


subtype_expr =  do
                    i <- sqBracks subtype_expr'
                    return i

subtype_expr' =     do
                        i <- identifier
                        is <- commaSep identifier
                        return (i,is)

di_tag =        do
                    d <- reserved "External"
                    return d
            <|>
                do
                    d <- reserved "InternalKnown"
                    return d
            <|>
                do
                    d <- reserved "InternalUnknown"
                    return d

timestamp_def =     do
                        reserved "TimeStamp"
                        reservedOp "="
                        t <- time
                        dot
                        dot
                        t' <- time
                        char '\n'
                        return (t:t')

maxruntime_def =    do
                        reserved "MaxRunTime"
                        reservedOp "="
                        t <- time
                        char '\n'
                        return t

act_inv_keys_dec =  do
                        a <- act_inv_key_pair
                        a' <- commaSep act_inv_key_pair
                        return (a,a')


act_inv_key_pair =  do
                        a <- parens act_inv_key_pair'
                        return a

act_inv_key_pair' =  do
                        i <- identifier
                        i' <- commaSep identifier
                        return (i:i')
-- Functions Section

functions = do
                reserved "#Functions"
                char '\n'
                f <- many functions_line
                return f

functions_line =    do
                        l <- explicit_func_line
                        return l
                 <|>
                    do
                        l <- symbolic_line
                        return l

explicit_func_line = do
                        f <- fn_def_lhs
                        reservedOp "="
                        i <- identifier
                        char '\n'
                        return (f,i)

fn_def_lhs = do
                i <- identifier
                f <- parens fn_def_lhs'
                return (i,f)

fn_def_lhs' = do
                f <- fn_def_arg
                fs <- commaSep fn_def_arg
                return (f:fs)

fn_def_arg =    do
                    i <- identifier
                    return i
            <|>
                do
                    i <- char '_'
                    return i

symbolic_line = do
                    reserved "symbolic"
                    i <- identifier
                    is <- commaSep identifier
                    char '\n'
                    return (i,is)

-- System Section
system = do
            reserved "#System"
            a <- many agent_dec
            w <-    do
                        w <- withdraw_dec
                        return w
                <|> do
                        spaces
                        return []
            g <-    do
                        g <- generate_system
                        return g
                <|> do
                        spaces
                        return []
            return (a,w,g)

agent_dec = do
                i <- instance_dec
                is <- semiSep instance_dec
                char '\n'
                return (i,is)

instance_dec = do
                    p <- process_name
                    i <- parens instance_dec'
                    return (p,i)

instance_dec' = do
                    i <- identifier
                    is <- commaSep identifier
                    return (i:is)

withdraw_dec = do
                    w <- reserved "WithdrawOption"
                    reservedOp "="
                    b <- withdraw_dec'
                    return (w,b)



withdraw_dec' =     do
                        b <- reserved "True"
                        char '\n'
                        return b
                <|>
                    do
                        b <- reserved "False"
                        char '\n'
                        return b

generate_system =   do
                        r <- reserved "GenerateSystem = True"
                        return r
                 <|>
                    do
                        r <- reserved "GenerateSystemForRepeatSection="
                        l <- line_no
                        reserved "to"
                        l' <- line_no
                        return (r,l,l')

-- Intruder Section

intruder = do
                reserved "#Intruder Knowledge"
                char '\n'
                reserved "Intruder"
                reservedOp "="
                i <- identifier
                char '\n'
                reserved "IntruderKnowledge"
                reservedOp "="
                ik <- many intruder'
                char '\n'
                ipd <-      do
                                ipd <- internal_proc_dec
                                return ipd
                        <|>
                            do
                                spaces
                                return []
                skd <-      do
                                skd <- stale_knowledge_dec
                                return skd
                        <|>
                            do
                                spaces
                                return []
                cr <-       do
                                cr <- crackable_dec
                                return cr
                        <|>
                            do
                                spaces
                                return []
                g <-        do
                                g <- guessable_dec
                                return g
                        <|>
                            do  spaces
                                return []
                u <-        do
                                u <- reserved "UnboundParallel = True"
                                return u
                        <|>
                            do
                                spaces
                                return []
                d <-        do
                                d <- deduction
                                return d
                        <|>
                            do
                                spaces
                                return []
                return (i,ik,ipd,skd,cr,g,u,d)

intruder' = do
                a <- atom
                a' <- commaSep atom
                return (a:a')

deduction = do
                reserved "forall"
                q <- quants
                dot
                m <- msg
                m' <- commaSep msg
                reservedOp "|-"
                m'' <- msg
                char '\n'
                return (q,m,m',m'')

internal_proc_dec = do
                        reserved "IntruderProcesses"
                        reservedOp "="
                        p <- process_name
                        p' <- commaSep process_name
                        char '\n'
                        return (p,p')

stale_knowledge_dec =   do
                            s <- reserved "StaleKnowledge"
                            reservedOp "="
                            b <- stale_knowledge_dec'
                            return (s,b)


stale_knowledge_dec' =      do
                                b <- reserved "True"
                                char '\n'
                                return b
                       <|>
                            do
                                b <- reserved "False"
                                char '\n'
                                return b

crackable_dec = do
                    reserved "Crackable"
                    reservedOp "="
                    cr <- crackable_type
                    cr' <- commaSep crackable_type
                    char '\n'
                    return (cr,cr')

crackable_type = do
                    t <- type_name
                    t' <-   do
                                t' <- parens time
                                return t'
                         <|>
                            do
                                spaces
                                return []
                    return (t,t')

guessable_dec = do
                    reserved "Guessable"
                    reservedOp "="
                    t <- type_name
                    t' <- commaSep type_name
                    char '\n'
                    return (t,t')

-- Secure Channels Section
-- TODO REDO
channels = do
                reserved "#Channels"
                char '\n'
                c <- channels'
                return c


channels' =         do
                        c <- many old_channel_spec
                        return c
               <|>
                    do
                        c <- many channel_spec
                        return c

old_channel_spec =      do
                            c <- reserved "Authenticated"
                            char '\n'
                            return c
                   <|>
                        do
                            c <- reserved "secret"
                            char '\n'
                            return c
                   <|>
                        do
                            c <- reserved "direct"
                            char '\n'
                            return c

channel_spec =          do
                            c <- msg_channel_spec
                            return c
                   <|>
                        do
                            c <- session_channel_spec
                            return c

msg_channel_spec = do
                       l <- line_no
                       m <- msg_channel_prop
                       char '\n'
                       return (l,m)

msg_channel_prop = do
                        c <-    do
                                    c <- reserved "C"
                                    return c
                            <|>
                                do
                                    spaces
                                    return []
                        nf <-   do
                                    nf <- reserved "NF"
                                    return nf
                            <|>
                                do
                                    spaces
                                    return []
                        nra <-  do
                                    nra <- msg_channel_prop'
                                    return nra
                            <|>
                                do
                                    spaces
                                    return []
                        nr <-   do
                                    nr <- msg_channel_prop''
                                    return nr
                            <|>
                                do
                                    spaces
                                    return []
                        return (c, nf, nra, nr)

msg_channel_prop' =     do
                                s <- reserved "NRA"
                                return s
                        <|> do
                                s <- reserved "NRA-"
                                return s

msg_channel_prop'' =    do
                                i <- reserved "NR"
                                return i
                        <|> do
                                i <-  reserved "NR-"
                                return i
session_channel_spec = do
                            s <- session_channel_spec'
                            s' <-   do
                                        s' <- session_channel_spec''
                                        return s'
                                 <|>
                                    do
                                        spaces
                                        return []

                            l <- line_no
                            l' <- commaSep line_no
                            char '\n'
                            return (s,s',l,l')

session_channel_spec' =     do
                                s <- reserved "Session"
                                return s
                        <|> do
                                s <- reserved "Stream"
                                return s

session_channel_spec'' =    do
                                i <- reserved "injective"
                                return i
                        <|> do
                                i <-  reserved "symmetric"
                                return i

