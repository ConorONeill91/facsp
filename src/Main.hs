

module Main (
    main
) where

import Term
import Exception
import Eval
import Text.ParserCombinators.Parsec
import Debug.Trace
import System.Directory
import System.IO
import System.Cmd
import Control.Monad (unless)
import Data.List (stripPrefix)
import System.Exit (exitFailure)

data Command = Load String
             | Term
             | Eval
             | Quit
             | Help
             | Unknown

command str = let res = words str
              in case res of
                   [":load",f] -> Load f
                   [":term"] -> Term
                   [":eval"] -> Eval
                   [":quit"] -> Quit
                   [":help"] -> Help
                   _ -> Unknown

help_message = "\n:load filename\t\tTo load the given Casper Script\n"++
               ":term\t\t\tTo print the current term\n"++
               ":eval\t\t\tTo evaluate the current term\n"++
               ":quit\t\t\tTo quit\n"++
               ":help\t\t\tTo print this message\n"


-- Entry point for main program

main = toplevel Nothing

toplevel :: Maybe Term -> IO ()
toplevel t = do putStr "FACSP> "
                hFlush stdout
                x <-  getLine
                case (command x) of
                   Load f -> do x <-  doesFileExist (f++".spl")
                                if x
                                   then do c <-  readFile (f++".spl")
                                           case parseProg c of
                                              Left s -> do putStrLn ("Could not parse term in file "++f++".spl: "++ show s)
                                                           toplevel t
                                              Right t -> do putStrLn ("Loading file: "++f++".spl")
                                                            toplevel (Just t)
                                   else do putStrLn ("No such casper script: "++f++".spl")
                                           toplevel Nothing
                   Term -> case t of
                              Nothing -> do putStrLn "No program loaded"
                                            toplevel t
                              Just u  -> do putStrLn (show u)
                                            toplevel t
                   Eval -> case t of
                              Nothing -> do putStrLn "No program loaded"
                                            toplevel t
                              Just u  -> f (free u) u
                                         where
                                         f [] u = do let (v,r,a) = evalnf u [] 0 0
                                                     putStrLn (show v)
                                                     putStrLn ("Reductions: " ++ show r)
                                                     putStrLn ("Allocations: " ++ show a)
                                                     toplevel t
                                         f (x:xs) u = do putStr (x++" = ")
                                                         hFlush stdout
                                                         l <-  getLine
                                                         case (parseExpr l) of
                                                            Left s -> do putStrLn ("Could not parse term: "++ show s)
                                                                         f (x:xs) u
                                                            Right t -> f xs (subst t (abstract x u))
                   Quit -> return ()
                   Help -> do putStrLn help_message
                              toplevel t
                   Unknown -> do putStrLn "Err: Could not parse command, type ':help' for a list of commands"
                                 toplevel t


