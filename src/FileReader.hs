-- Sample file reader module
-- Will be used to read Casper Script
module FileReader
    where

import System.IO
import System.Environment
import System.Directory

sayHello = do
    putStrLn "What's your name: "
    name <- getLine
    putStrLn $ "Hello " ++ name -- $ is used instead of the parentheses

-- File IO
-- Write to a file
writeToFile = do
    theFile <- openFile "test.txt" WriteMode 	-- Open the file using WriteMode
    hPutStrLn theFile ("Random line of text") 	-- Put the text in the file
    hClose theFile     -- hClose theFile


readFromFile = do
    theFile2 <- openFile "test.txt" ReadMode 	-- Open the file using ReadMode
    contents <- hGetContents theFile2 	-- Get the contents of the file
    putStr contents
    hClose theFile2 -- Close the file